#ifndef _DPYDRIVE_
#define _DPYDRIVE_

#include <gd32f10x_libopt.h>
#include "spi.h"

//Раскоментируйте строку ниже, если используете тачскрин
//#define DPY_USE_TOUCH

//Раскоментируйте строку ниже, если используете DMA
//#define DPY_USE_DMA

/*
	-------------------START---------------------
	Сконфигурируйте переменные под свои параметры
	---------------------------------------------
*/

#define DPY_CONFIG_SPI_PSC					SPI_PSC_16

#define DPY_CONFIG_BL_PORT					GPIOB
#define DPY_CONFIG_BL_PIN						GPIO_PIN_1

#define DPY_CONFIG_A0_PORT					GPIOA
#define DPY_CONFIG_A0_PIN						GPIO_PIN_15

#define DPY_CONFIG_RESET_PORT				GPIOA
#define DPY_CONFIG_RESET_PIN				GPIO_PIN_1

#ifdef DPY_USE_TOUCH
	#define DPY_CONFIG_TOUCH_PEN_PORT			GPIOA
	#define DPY_CONFIG_TOUCH_PEN_PIN			GPIO_PIN_4
	
	#define DPY_CONFIG_TOUCH_CS_PORT			GPIOA
	#define DPY_CONFIG_TOUCH_CS_PIN				GPIO_PIN_9
#endif


#define DPY_CONFIG_SPI							SPI0

#define DPY_CONFIG_SPI_CS_PORT			GPIOA
#define DPY_CONFIG_SPI_CS_PIN				GPIO_PIN_8

#define DPY_SCREEN_WIDTH		240
#define DPY_SCREEN_HEIGHT		320

/*
	---------------------END---------------------
	Сконфигурируйте переменные под свои параметры
	---------------------------------------------
*/

/*
	--------------START----------------
	Набор доступных комманд для ILI9341
	-----------------------------------
*/
#define DPY_CMD_SOFTWARE_RESET				0x01
#define DPY_CMD_ENTER_SLEEP_MODE			0x10
#define DPY_CMD_OUT_SLEEP_MODE				0x11
#define DPY_CMD_GAMMA_SET							0x26
#define DPY_CMD_DISPLAY_OFF						0x28
#define DPY_CMD_DISPLAY_ON						0x29
#define DPY_CMD_COLUMN_ADDR_SET				0x2A
#define DPY_CMD_PAGE_ADDR_SET					0x2B
#define DPY_CMD_MEMORY_WRITE					0x2C
#define DPY_CMD_COLOR_SET							0x2D
#define DPY_CMD_PAGE_ADDR_SET					0x2B
#define DPY_CMD_MEMORY_WRITE_CON			0x3C

#define DPY_CMD_POWER_CONTROL_1				0xC0
#define DPY_CMD_POWER_CONTROL_2				0xC1
#define DPY_CMD_VCOM_CONTROL_1				0xC5
#define DPY_CMD_VCOM_CONTROL_2				0xC7
#define DPY_CMD_MEM_ACCES_CONTROL			0x36
#define DPY_CMD_PIXEL_FORMAT_SET			0x3A
#define DPY_CMD_FRAME_RATE_CONTROL		0xB1
#define DPY_CMD_DISP_FUNC_CONTROL			0xB6
#define DPY_CMD_GAMMA_SET							0x26
#define DPY_CMD_POS_GAMMA_CORRECT			0xE0
#define DPY_CMD_NEG_GAMMA_CORRECT			0xE1

#define DPY_CMD_POWER_CONTROL_A				0xCB
#define DPY_CMD_POWER_CONTROL_B				0xCF
#define DPY_CMD_TIMING_CONTROL_A			0xE8
#define DPY_CMD_TIMING_CONTROL_B			0xEA
#define DPY_CMD_POWER_SEQ_CONTROL			0xED
#define DPY_CMD_PUMP_RATIO_CONTROL		0xF7
#define DPY_CMD_ENABLE_3G							0xF2

/*
	---------------END-----------------
	Набор доступных комманд для ILI9341
	-----------------------------------
*/

#define DPY_CONNECT_SELECT_DISPLAY		0
#define DPY_CONNECT_SELECT_TOUCH			1

#define DPY_CONNECT_MODE_COMMAND			0
#define DPY_CONNECT_MODE_DATA					1

#define DPY_CONNECT_DISP_CS(X)					gpio_bit_write(DPY_CONFIG_SPI_CS_PORT, DPY_CONFIG_SPI_CS_PIN, X)
#define DPY_CONNECT_DISP_RESET(X)				gpio_bit_write(DPY_CONFIG_RESET_PORT, DPY_CONFIG_RESET_PIN, X)
#define DPY_CONNECT_DISP_A0(X)					gpio_bit_write(DPY_CONFIG_A0_PORT, DPY_CONFIG_A0_PIN, X)

#ifdef DPY_USE_TOUCH
	#define DPY_CONNECT_TOUCH_CS(X)					gpio_bit_write(DPY_CONFIG_TOUCH_CS_PORT, DPY_CONFIG_TOUCH_CS_PIN, X)
#endif

#define DPY_CONNECT_SPI_TXBUFFER_MAX_LENGHT		20
#define DPY_CONNECT_SPI_TXBUFFER_EMPTY				0

#ifdef DPY_USE_TOUCH
	#define DPY_CONNECT_SPI_RXBUFFER_MAX_LENGHT	10
#endif

typedef struct status{
	uint8_t txBufferCounter;
	uint8_t select;
	uint8_t mode;
} ConnectStatus;

#define DPY_OBJ_PIXEL_COLOR_SET(R, G, B)		R << 11 | (G & 0x3F) << 5 | (B & 0x1F)

typedef unsigned short ObjPixel;

void dpy_init(void);
void dpy_send(uint8_t command, uint8_t select, uint8_t* data, uint8_t data_size);
void dpy_send_raw(uint8_t select, uint8_t data, uint8_t data_size);

void dpy_draw_window(uint16_t _xst, uint16_t _yst, uint16_t _xend, uint16_t _yend, uint8_t _color);
void dpy_draw_text(uint16_t _x, uint16_t _y, uint8_t _color, uint8_t* _text);

void dpy_event_spi(void);


#endif
