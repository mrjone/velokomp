#ifndef _AXELDRIVE_
#define _AXELDRIVE_

#include <gd32f10x_libopt.h>
#include "spi.h"

/*
	-------------------START---------------------
	Сконфигурируйте переменные под свои параметры
	---------------------------------------------
*/

#define AXEL_CONFIG_SPI_PSC					SPI_PSC_16




#define AXEL_CONFIG_SPI							SPI0

#define AXEL_CONFIG_SPI_MISO_PORT		GPIOA
#define AXEL_CONFIG_SPI_MISO_PIN		GPIO_PIN_6

#define AXEL_CONFIG_SPI_MOSI_PORT		GPIOA
#define AXEL_CONFIG_SPI_MOSI_PIN		GPIO_PIN_7

#define AXEL_CONFIG_SPI_SCK_PORT		GPIOA
#define AXEL_CONFIG_SPI_SCK_PIN			GPIO_PIN_5

#define AXEL_CONFIG_SPI_CS_PORT			GPIOA
#define AXEL_CONFIG_SPI_CS_PIN			GPIO_PIN_10

/*
	---------------------END---------------------
	Сконфигурируйте переменные под свои параметры
	---------------------------------------------
*/
/*
	--------------START----------------
	      Адреса памяти ADXL345
	-----------------------------------
*/
#define AXEL_ADDR_DEVID			      	0x00

#define AXEL_ADDR_THRESH_TAP	      0x1D

#define AXEL_ADDR_OFSX				      0x1E
#define AXEL_ADDR_OFSY					    0x1F
#define AXEL_ADDR_OFSZ				    	0x20

#define AXEL_ADDR_DUR    		      	0x21
#define AXEL_ADDR_Latent 			    	0x22
#define AXEL_ADDR_Window				   	0x23

#define AXEL_ADDR_THRESH_ACT				0x24
#define AXEL_ADDR_THRESH_INACT			0x25

#define AXEL_ADDR_TIME_INACT	    	0x26

#define AXEL_ADDR_ACT_INACT_CTL	  	0x27
#define AXEL_ADDR_THRESH_FF 			  0x28
#define AXEL_ADDR_TIME_FF				    0x29

#define AXEL_ADDR_TAP_AXES				  0x2A
#define AXEL_ADDR_ACT_TAP_STATUS		0x2B

#define AXEL_ADDR_BW_RATE			      0x2C

#define AXEL_ADDR_POWER_CTL		      0x2D

#define AXEL_ADDR_INT_ENABLE		    0x2E
#define AXEL_ADDR_INT_MAP				  	0x2F
#define AXEL_ADDR_INT_SOURCE				0x30

#define AXEL_ADDR_DATA_FORMAT		  	0x31
#define AXEL_ADDR_DATAX0						0x32
#define AXEL_ADDR_DATAX1						0x33
#define AXEL_ADDR_DATAY0						0x34
#define AXEL_ADDR_DATAY1						0x35
#define AXEL_ADDR_DATAZ0						0x36
#define AXEL_ADDR_DATAZ1						0x37

#define AXEL_ADDR_FIFO_CTL					0x38
#define AXEL_ADDR_FIFO_STATUS		  	0x39

/*
	---------------END-----------------
	      Адреса памяти ADXL345
	-----------------------------------
*/

#define AXEL_CONNECT_DISP_CS(X)				  	gpio_bit_write(DPY_CONFIG_SPI_CS_PORT, DPY_CONFIG_SPI_CS_PIN, X)
#define AXEL_CONNECT_DISP_RESET(X)				gpio_bit_write(DPY_CONFIG_RESET_PORT, DPY_CONFIG_RESET_PIN, X)

#define AXEL_COMMAND_RW_pos					7
#define AXEL_COMMAND_MB_pos					6
#define AXEL_COMMAND_MAKE(RW, MB, ADDR)		RW << AXEL_COMMAND_RW_pos | MB << AXEL_COMMAND_MB_pos | ADDR & 0x1F

void axel_init(void);

void axel_event_spi(void);



#endif
