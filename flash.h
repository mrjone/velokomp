#ifndef _FLASHDRIVE_
#define _FLASHDRIVE_

#include <gd32f10x_libopt.h>
#include "spi.h"

/*
	-------------------START---------------------
	Сконфигурируйте переменные под свои параметры
	---------------------------------------------
*/

#define FLASH_CONFIG_SPI_PSC					SPI_PSC_16




#define FLASH_CONFIG_SPI							SPI0

#define FLASH_CONFIG_SPI_MISO_PORT		GPIOA
#define FLASH_CONFIG_SPI_MISO_PIN 		GPIO_PIN_6

#define FLASH_CONFIG_SPI_MOSI_PORT		GPIOA
#define FLASH_CONFIG_SPI_MOSI_PIN	  	GPIO_PIN_7

#define FLASH_CONFIG_SPI_SCK_PORT 		GPIOA
#define FLASH_CONFIG_SPI_SCK_PIN			GPIO_PIN_5

#define FLASH_CONFIG_SPI_CS_PORT			GPIOA
#define FLASH_CONFIG_SPI_CS_PIN		  	GPIO_PIN_10

/*
	---------------------END---------------------
	Сконфигурируйте переменные под свои параметры
	---------------------------------------------
*/
/*
	----------------------------START-------------------------------
										  		Команды памяти
	----------------------------------------------------------------
*/
#define FLASH_ADDR_Write_Enable                                 0x06
#define FLASH_ADDR_Write_Disable                                0x04
#define FLASH_ADDR_Volatile_SR_Write_Enable                     0x50
#define FLASH_ADDR_Read_Status_Register                         0x05 
#define FLASH_ADDR_Read_Status_Register_1                       0x35 
#define FLASH_ADDR_Write_Status_Register                        0x01 
#define FLASH_ADDR_Read_Data                                    0x03
#define FLASH_ADDR_Fast_Read                                    0x0B
#define FLASH_ADDR_Continuous_Read_Mode_Reset                   0xFF
#define FLASH_ADDR_Page_Program                                 0x02 
#define FLASH_ADDR_Sector_Erase                                 0x20
#define FLASH_ADDR_Block_Erase_32K                              0x52 
#define FLASH_ADDR_Block_Erase_64K                              0xD8
#define FLASH_ADDR_Chip_Erase                                   0x60
#define FLASH_ADDR_Enable_Reset                                 0x66
#define FLASH_ADDR_Reset                                        0x99
#define FLASH_ADDR_Set_Burst_with_Wrap                          0x77
#define FLASH_ADDR_Program_Erase_Suspend                        0x75
#define FLASH_ADDR_Program_Erase_Resume                         0x7A
#define FLASH_ADDR_Deep_Power_Down                              0xB9 
#define FLASH_ADDR_Release_From_Deep_Power_Down                 0xAB
#define FLASH_ADDR_Manufacturer_Device_ID                       0x90 
#define FLASH_ADDR_High_Performance_Mode                        0xA3
#define FLASH_ADDR_Read_Serial_Flash_Discov_Param               0x5A 
#define FLASH_ADDR_Read_Identification                          0x9F
#define FLASH_ADDR_Read_Unique_ID                               0x4B 
#define FLASH_ADDR_Erase_Security_Registers                     0x44 
#define FLASH_ADDR_Program_Security_Registers                   0x42
#define FLASH_ADDR_Read_Security_Registers                      0x48 

/*
		----------------------------END-------------------------------
													Команды памяти
	----------------------------------------------------------------
*/

#define FLASH_ADDR_



#define FLASH_CONNECT_DISP_CS(X)				  	gpio_bit_write(FLASH_CONFIG_SPI_CS_PORT, FLASH_CONFIG_SPI_CS_PIN, X)
#define FLASH_CONNECT_DISP_RESET(X)			   	gpio_bit_write(FLASH_CONFIG_RESET_PORT, FLASH_CONFIG_RESET_PIN, X)



void flash_init(void);

void flash_event_spi(void);

void flash_reset(void);

#endif
