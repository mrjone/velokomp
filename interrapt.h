#ifndef _INTERRAPT_
#define _INTERRAPT_

#include <gd32f10x_libopt.h>

void EXTI5_9_IRQHandler(void);
void TIMER3_IRQHandler(void);

#endif
