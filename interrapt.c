#include "interrapt.h"
#include "speed.h"

void EXTI5_9_IRQHandler(void)
{
	if (exti_interrupt_flag_get(EXTI_5))
	{
		//TODO: BREAK evenet
	}
	
	if (exti_interrupt_flag_get(EXTI_6))
	{
		speed_control_event_rotation();
	}
}


void TIMER3_IRQHandler(void)
{
	speed_control_event_timer();
}

