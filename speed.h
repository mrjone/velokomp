#ifndef _SPEED_
#define _SPEED_

#include <gd32f10x_libopt.h>

#define Pi			3.141592f

#define HOLL_PIN	GPIO_PIN_6
#define HOLL_PORT	GPIOB

#define SPEED_CONTROL_TIMER_PERIOD	5400
#define SPEED_CONTROL_TIMER_PRESC		999

void speed_control_event_rotation(void);
void speed_control_event_timer(void);
void speed_control_init(void);
unsigned int speed_control_get(void);

#endif
