#include "dpydrive.h"

static volatile uint16_t dpy_connect_txBuffer[DPY_CONNECT_SPI_TXBUFFER_MAX_LENGHT];

#ifdef DPY_USE_TOUCH
	volatile uint16_t dpy_connect_rxBuffer[DPY_CONNECT_SPI_RXBUFFER_MAX_LENGHT];
#endif

volatile ConnectStatus dpy_connect_status;

extern volatile spi_connect_info spi_info;

void dpy_init(void)
{	
	spi_parameter_struct init_params;
		init_params.trans_mode = SPI_TRANSMODE_FULLDUPLEX;
		init_params.device_mode = SPI_MASTER;
		init_params.frame_size = SPI_FRAMESIZE_8BIT;
		init_params.clock_polarity_phase = SPI_CK_PL_LOW_PH_1EDGE;
		init_params.nss = SPI_NSS_SOFT;
		init_params.prescale = DPY_CONFIG_SPI_PSC;
		init_params.endian = SPI_ENDIAN_MSB;
	spi_init(DPY_CONFIG_SPI, &init_params);
	
	dpy_connect_status.txBufferCounter = 0;
	dpy_connect_status.mode = DPY_CONNECT_MODE_COMMAND;
	
	spi_i2s_interrupt_enable(DPY_CONFIG_SPI, SPI_I2S_INT_TBE);
	#ifdef DPY_USE_TOUCH
		spi_i2s_interrupt_enable(DPY_CONFIG_SPI, SPI_I2S_INT_RBNE);
	#endif
	spi_enable(DPY_CONFIG_SPI);
	
	DPY_CONNECT_DISP_RESET(SET);
	DPY_CONNECT_DISP_CS(SET);
	#ifdef DPY_USE_TOUCH
		DPY_CONNECT_TOUCH_CS(SET);
	#endif
	
	//TODO: Запуск дисплея
	dpy_send(DPY_CMD_SOFTWARE_RESET, DPY_CONNECT_SELECT_DISPLAY, 0, 0);
	//TODO: задержка 50 мс
	
	uint8_t data_com[16];
	
	data_com[0] = 0x02;
	data_com[1] = 0x34;
	data_com[2] = 0x00;
	data_com[3] = 0x2C;
	data_com[4] = 0x39;
	dpy_send(DPY_CMD_POWER_CONTROL_A, DPY_CONNECT_SELECT_DISPLAY, data_com, 5);
	
	data_com[0] = 0x30;
	data_com[1] = 0xC1;
	data_com[2] = 0x00;
	dpy_send(DPY_CMD_POWER_CONTROL_B, DPY_CONNECT_SELECT_DISPLAY, data_com, 3);
	
	data_com[0] = 0x78;
	data_com[1] = 0x00;
	data_com[2] = 0x85;
	dpy_send(DPY_CMD_TIMING_CONTROL_A, DPY_CONNECT_SELECT_DISPLAY, data_com, 3);
	
	data_com[0] = 0x00;
	data_com[1] = 0x00;
	dpy_send(DPY_CMD_TIMING_CONTROL_B, DPY_CONNECT_SELECT_DISPLAY, data_com, 2);
	
	data_com[0] = 0x81;
	data_com[1] = 0x12;
	data_com[2] = 0x03;
	data_com[3] = 0x64;
	dpy_send(DPY_CMD_POWER_SEQ_CONTROL, DPY_CONNECT_SELECT_DISPLAY, data_com, 4);
	
	data_com[0] = 0x20;
	dpy_send(DPY_CMD_PUMP_RATIO_CONTROL, DPY_CONNECT_SELECT_DISPLAY, data_com, 1);
	
	data_com[0] = 0x23;
	dpy_send(DPY_CMD_POWER_CONTROL_1, DPY_CONNECT_SELECT_DISPLAY, data_com, 1);
	
	data_com[0] = 0x10;
	dpy_send(DPY_CMD_POWER_CONTROL_2, DPY_CONNECT_SELECT_DISPLAY, data_com, 1);
	
	data_com[0] = 0x28;
	data_com[1] = 0x3E;
	dpy_send(DPY_CMD_VCOM_CONTROL_1, DPY_CONNECT_SELECT_DISPLAY, data_com, 2);
	
	data_com[0] = 0x86;
	dpy_send(DPY_CMD_VCOM_CONTROL_2, DPY_CONNECT_SELECT_DISPLAY, data_com, 1);
	
	data_com[0] = 0x48;
	dpy_send(DPY_CMD_MEM_ACCES_CONTROL, DPY_CONNECT_SELECT_DISPLAY, data_com, 1);
	
	data_com[0] = 0x55;
	dpy_send(DPY_CMD_PIXEL_FORMAT_SET, DPY_CONNECT_SELECT_DISPLAY, data_com, 1);
	
	data_com[0] = 0x18;
	data_com[1] = 0x00;
	dpy_send(DPY_CMD_FRAME_RATE_CONTROL, DPY_CONNECT_SELECT_DISPLAY, data_com, 2);
	
	data_com[0] = 0x27;
	data_com[1] = 0x82;
	data_com[2] = 0x08;
	dpy_send(DPY_CMD_DISP_FUNC_CONTROL, DPY_CONNECT_SELECT_DISPLAY, data_com, 3);
	
	data_com[0] = 0x00;
	dpy_send(DPY_CMD_ENABLE_3G, DPY_CONNECT_SELECT_DISPLAY, data_com, 1);
	
	data_com[0] = 0x01;
	dpy_send(DPY_CMD_GAMMA_SET, DPY_CONNECT_SELECT_DISPLAY, data_com, 1);
	
	data_com[0] = 0x00;
	data_com[1] = 0x09;
	data_com[2] = 0x0E;
	data_com[3] = 0x03;
	data_com[4] = 0x10;
	data_com[5] = 0x07;
	data_com[6] = 0x37;
	data_com[7] = 0xF1;
	data_com[8] = 0x4E;
	data_com[9] = 0x08;
	data_com[10] = 0x0E;
	data_com[11] = 0x0C;
	data_com[12] = 0x2B;
	data_com[13] = 0x31;
	data_com[14] = 0x0F;
	dpy_send(DPY_CMD_POS_GAMMA_CORRECT, DPY_CONNECT_SELECT_DISPLAY, data_com, 15);
	
	data_com[0] = 0x0F;
	data_com[1] = 0x36;
	data_com[2] = 0x31;
	data_com[3] = 0x0C;
	data_com[4] = 0x0F;
	data_com[5] = 0x08;
	data_com[6] = 0x48;
	data_com[7] = 0xC1;
	data_com[8] = 0x31;
	data_com[9] = 0x07;
	data_com[10] = 0x11;
	data_com[11] = 0x03;
	data_com[12] = 0x14;
	data_com[13] = 0x0E;
	data_com[14] = 0x00;
	dpy_send(DPY_CMD_NEG_GAMMA_CORRECT, DPY_CONNECT_SELECT_DISPLAY, data_com, 15);
	
	dpy_send(DPY_CMD_OUT_SLEEP_MODE, DPY_CONNECT_SELECT_DISPLAY, 0, 0);
	//TODO: Ожидание 100 мс
	
	dpy_send(DPY_CMD_DISPLAY_ON, DPY_CONNECT_SELECT_DISPLAY, 0, 0);
	
	//TODO: Установить поворот экрана
}

void dpy_event_spi(void)
{
	if (spi_i2s_interrupt_flag_get(DPY_CONFIG_SPI, SPI_I2S_INT_TBE))
		//Обработка прерывания: Опустошение буфера
	{
		if (dpy_connect_status.txBufferCounter != DPY_CONNECT_SPI_TXBUFFER_EMPTY)
			//Если переданны не все данные, то продолжается конвеерная передача из буфера
		{
			switch (dpy_connect_status.select)
			{
				case DPY_CONNECT_SELECT_DISPLAY:
					if (dpy_connect_status.mode == DPY_CONNECT_MODE_COMMAND)
						//После отправки команды необходимо сбросить режим
						//передачи комманд и перевести в режим передачи данных
					{
						dpy_connect_status.mode = DPY_CONNECT_MODE_DATA;
						DPY_CONNECT_DISP_A0(SET);
					}
					dpy_connect_status.txBufferCounter--;
					spi_i2s_data_transmit(DPY_CONFIG_SPI, dpy_connect_txBuffer[dpy_connect_status.txBufferCounter]);
					break;
				case DPY_CONNECT_SELECT_TOUCH:
					#ifdef DPY_USE_TOUCH
					#endif
					break;
			}
		}
		
		//TODO: Буфер передачи пустой
	}
	#ifdef DPY_USE_TOUCH
		if (spi_i2s_interrupt_flag_get(DPY_CONFIG_SPI, SPI_I2S_INT_RBNE))
		{
			//TODO: Буфер приёма не пустой
		}
	#endif
}

void dpy_send(uint8_t command, uint8_t select, uint8_t* data, uint8_t data_size)
{
	while(dpy_connect_status.txBufferCounter != DPY_CONNECT_SPI_TXBUFFER_EMPTY);
	//Ожидание доступа на передачу
	if (data != 0)
	{
		if (data_size > DPY_CONNECT_SPI_TXBUFFER_MAX_LENGHT)
			data_size = DPY_CONNECT_SPI_TXBUFFER_MAX_LENGHT;
		
		for (uint8_t i = 0; i < data_size; i++)
		{
			dpy_connect_txBuffer[i] = data[i];
		}
	}
	
	if (select == DPY_CONNECT_SELECT_DISPLAY)
	{
		#ifdef DPY_USE_TOUCH
			DPY_CONNECT_TOUCH_CS(SET);
		#endif
		DPY_CONNECT_DISP_CS(RESET);
	}
	else	//DPY_CONNECT_SELECT_TOUCH
	{
		DPY_CONNECT_DISP_CS(SET);
		#ifdef DPY_USE_TOUCH
			DPY_CONNECT_TOUCH_CS(RESET);
		#endif
	}
	
	dpy_connect_status.txBufferCounter = data_size;
	dpy_connect_status.mode = DPY_CONNECT_MODE_COMMAND;
	DPY_CONNECT_DISP_A0(RESET);
	spi_i2s_data_transmit(DPY_CONFIG_SPI, command);
	//Отправка первого командного байта
}

void dpy_send_raw(uint8_t select, uint8_t data, uint8_t data_size)
{
	while(dpy_connect_status.txBufferCounter != DPY_CONNECT_SPI_TXBUFFER_EMPTY);
	//Ожидание доступа на передачу
	
	if (select == DPY_CONNECT_SELECT_DISPLAY)
	{
		#ifdef DPY_USE_TOUCH
			DPY_CONNECT_TOUCH_CS(SET);
		#endif
		DPY_CONNECT_DISP_CS(RESET);
	}
	else	//DPY_CONNECT_SELECT_TOUCH
	{
		DPY_CONNECT_DISP_CS(SET);
		#ifdef DPY_USE_TOUCH
			DPY_CONNECT_TOUCH_CS(RESET);
		#endif
	}
	
	dpy_connect_status.txBufferCounter = data_size;
	dpy_connect_status.mode = DPY_CONNECT_MODE_COMMAND;
	DPY_CONNECT_DISP_A0(RESET);
	spi_i2s_data_transmit(DPY_CONFIG_SPI, DPY_CMD_MEMORY_WRITE);
	//Отправка первого командного байта
}

void dpy_draw_window(uint16_t _xst, uint16_t _yst, uint16_t _xend, uint16_t _yend, uint8_t _color)
{
	if (_xst > DPY_SCREEN_WIDTH || _yst > DPY_SCREEN_HEIGHT
			|| _xend > DPY_SCREEN_WIDTH || _yend > DPY_SCREEN_HEIGHT)
		return;
	
	uint8_t temp[5];
	
	temp[0] = (_xst << 8) & 0xFF;
	temp[1] = _xst & 0xFF;
	temp[2] = (_xend << 8) & 0xFF;
	temp[3] = _xend & 0xFF;
	dpy_send(DPY_CMD_COLUMN_ADDR_SET, DPY_CONNECT_SELECT_DISPLAY, temp, 4);
	
	temp[0] = (_yst << 8) & 0xFF;
	temp[1] = _yst & 0xFF;
	temp[2] = (_yend << 8) & 0xFF;
	temp[3] = _yend & 0xFF;
	dpy_send(DPY_CMD_PAGE_ADDR_SET, DPY_CONNECT_SELECT_DISPLAY, temp, 4);
	
	
}

void dpy_draw_text(uint16_t _x, uint16_t _y, uint8_t _color, uint8_t* _text)
{
}
