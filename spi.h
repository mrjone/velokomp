#ifndef _SPI_
#define _SPI_

#include <gd32f10x_libopt.h>

#define SPI_INFO_SS_FLASH		0
#define SPI_INFO_SS_DISP		1
#define SPI_INFO_SS_TOUCH		2
#define SPI_INFO_SS_AXEL		3

#define SPI_INFO_MAX_BUFFER_TX	100
#define SPI_INFO_MAX_BUFFER_RX	50

typedef struct{
	uint16_t txBufferCounter;
	uint16_t rxBufferCounter;
	uint8_t selectSlave;
} spi_connect_info;

void SPI0_IRQHandler(void);

#endif