#include "speed.h"

static volatile unsigned int count_rotation;
static volatile unsigned int wheel_radius = 100; //позиция датчика от центра колеса в мм
static volatile unsigned int main_wheel_radius = 351; //радиус колеса вмесе с покрышкой в мм
static volatile unsigned int speed_value;
static volatile unsigned int intermediate_mileage; //пробег за поездку
static volatile unsigned int total_mileage; // общий пробег

void speed_control_event_rotation(void)
{
	count_rotation++;
}

void speed_control_event_timer(void)
{
	speed_value = (((2*Pi*wheel_radius*10)/0.1f)*0.0036); //скорость в км/ч
	intermediate_mileage = (((2*Pi*main_wheel_radius*10)*count_rotation)*1000); // пробег за поездку в км
	total_mileage = +intermediate_mileage; // общий пробег в км
	
}
void speed_control_init(void)
{
	rcu_apb1_clock_config(RCU_APB1_CKAHB_DIV2);
	rcu_periph_clock_enable(RCU_TIMER3);
	timer_parameter_struct timer_speed_counter_params;
		timer_speed_counter_params.alignedmode = TIMER_COUNTER_EDGE;
		timer_speed_counter_params.clockdivision = TIMER_CKDIV_DIV1;
		timer_speed_counter_params.counterdirection = TIMER_COUNTER_UP;
		timer_speed_counter_params.period = SPEED_CONTROL_TIMER_PERIOD;
		timer_speed_counter_params.prescaler = SPEED_CONTROL_TIMER_PRESC;
		timer_speed_counter_params.repetitioncounter = 0;
	timer_init(TIMER3, &timer_speed_counter_params);
	timer_interrupt_enable(TIMER3, TIMER_INT_UP);
	timer_enable(TIMER3);
	
	
	gpio_init(HOLL_PORT, 
		GPIO_MODE_IN_FLOATING, 
		GPIO_OSPEED_2MHZ, 
		HOLL_PIN);
	
	exti_init(EXTI_6, 
		EXTI_INTERRUPT, 
		EXTI_TRIG_FALLING);
	exti_interrupt_enable(EXTI_6);
}

unsigned int speed_control_get(void)
{
	return speed_value;
}
